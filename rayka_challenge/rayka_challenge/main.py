import boto3

from django.conf import settings


# Test Purpose
item = {
    "id": "/devices/id1",
    "deviceModel": "/devicemodels/id1",
    "name": "Sensor",
    "note": "Testing a sensor.",
    "serial": "A020000102",
}


class DynamoDB:
    def __init__(
        self, *, region_name, aws_access_key_id, aws_secret_access_key, service_name
    ):
        self.dynamodb = boto3.resource(
            service_name="dynamodb",
            region_name=region_name,
            aws_access_key_id=aws_access_key_id,
            aws_secret_access_key=aws_secret_access_key,
        )

    def get_item(self, *, item_id):
        table = self.dynamodb.Table("Device")
        return table.get_item(Key={"id": item_id})

    def put_item(self, *, item):
        table = self.dynamodb.Table("Device")
        return table.put_item(Item=item)

    def create_device_table(self):
        self.dynamodb.create_table(
            TableName="Device",
            AttributeDefinitions=[
                {
                    "AttributeName": "id",
                    "AttributeType": "S",
                },
            ],
            KeySchema=[
                {
                    "AttributeName": "id",
                    "KeyType": "HASH",
                },
            ],
            ProvisionedThroughput={
                "ReadCapacityUnits": 10,
                "WriteCapacityUnits": 10,
            },
        )


db = DynamoDB(
    # endpoint_url="dynamodb.us-east-1.amazonaws.com",
    service_name="dynamodb",
    region_name="us-east-1",
    aws_access_key_id=settings.AWS_ACCESS,
    aws_secret_access_key=settings.AWS_SECRET,
)
