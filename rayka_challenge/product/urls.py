from django.urls import path

from .views import *

urlpatterns = [
    path("devices", CreateProduct.as_view()),
    path("devices/<str:id>", ProductView.as_view()),
]
