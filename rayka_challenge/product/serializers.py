from rest_framework import serializers


class ProductSerializer(serializers.Serializer):
    id = serializers.CharField(max_length=200)
    deviceModel = serializers.CharField(max_length=200)
    name = serializers.CharField(max_length=200)
    note = serializers.CharField(max_length=200)
    serial = serializers.CharField(max_length=200)
