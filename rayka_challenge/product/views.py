from django.shortcuts import render

from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView

from .serializers import ProductSerializer
from rayka_challenge.main import db


class CreateProduct(APIView):
    def post(self, request):

        serializer = ProductSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        db.put_item(item=serializer.data)

        return Response(data=serializer.data, status=status.HTTP_201_CREATED)


class ProductView(APIView):
    def get(self, request, id):
        product = db.get_item(item_id=id)
        product = product.get("Item")
        if product is None:
            return Response(data="device not found", status=status.HTTP_404_NOT_FOUND)

        serializer = ProductSerializer(product)

        return Response(data=serializer.data, status=status.HTTP_200_OK)
